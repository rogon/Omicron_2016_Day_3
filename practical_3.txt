Network Biology Fundamentals

Schedule details:

------------------------------------------------------------------------------
Day 3
------------------------------------------------------------------------------

Theory:
09:00-10:00 - Reconstructing regulatory networks
10:00-11:00 - Overview of common methods for data integration
16:00 -       Practical session


upload materias to GIT

Practical:
1. Enrichment analysis
	a. Jepetto and Reactome 
	b. BinGO and ClueGO in Cytoscape
	c. ClusterProfiler in R

2. Data integration
	Enrichment Map
	ClueGO/CluePedia
	Kernels - R

3. Gene Regulatory networks
	iRegulon
	CyTargetLinker


Środa: Justyna Totoń-Żurańska i Agnieszka Borys
Dane: ekspresja miRNA (NGS) oraz metylacja (mikromacierze)


Data types
Sourced of data
Networks are adjacency matrices, thus they represent binary relationships between nodes
Because we're dealing with matrices, linear algebra applies, thus providing a well mathematically founded way of representing and transforming data.
Graph study